<?php

require_once(__DIR__ . '/vendor/autoload.php');
use DeviceDetector\DeviceDetector;
use DeviceDetector\Parser\Device\DeviceParserAbstract;

DeviceParserAbstract::setVersionTruncation(DeviceParserAbstract::VERSION_TRUNCATION_NONE);
$dd = new DeviceDetector($_SERVER['HTTP_USER_AGENT']);
$dd->setCache(new Doctrine\Common\Cache\PhpFileCache(__DIR__ .'/tmp/'));
$dd->setYamlParser(new \DeviceDetector\Yaml\Symfony());
$dd->discardBotInformation();
$dd->skipBotDetection();

$dd->parse();

$validStates = [
    0 => 'Sales',
    1 => 'Marketing',
    2 => 'Development',
    3 => 'Management',
    4 => 'International Affairs',
];

$browserWidth = '';

switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        $validate = validate($_POST, $validStates);
        header('Content-Type: application/json');
        if ($validate['status'] == 'success') {
            sendMail($_POST, $validStates);
        }
        echo json_encode($validate);
        break;
    default:
        if (isset($_COOKIE['browserWidth'])) {
            if ($_COOKIE['browserWidth'] >= 1024) {
                $version = '/final2.html';
            } else {
                $version = '/mobile3.html';
            }
        } else {
            switch ($dd->getDevice()) {
                case 1:
                    $version = '/mobile3.html';
                    setcookie('browserWidth', 700);
                    break;
                case 2:
                    $version = '/final2.html';
                    setcookie('browserWidth', 1300);
                    break;
                default:
                    $version = '/final2.html';
            }
        }
        require __DIR__ . $version;
}

// Sending Mail
function sendMail ($inputs, $validStates)
{
    $messageBody = parseTemplate($inputs);
    $transport = (new Swift_SmtpTransport('smtp.zoho.com', 587, 'tls'))
        ->setUsername('dev@theinnotech.net')
        ->setPassword('dev@innotech098poi');
    $mailer = new Swift_Mailer($transport);
    $message = (new Swift_Message('innotech contact us'))
        ->setFrom(['dev@theinnotech.net' => 'innotech website'])
        ->setTo(['info@theinnotech.net' => 'innotech support'])
        ->setBody($messageBody)
        ->setContentType('text/html');
    return $mailer->send($message);
}

/**
 * @return string
 */
function parseTemplate($inputs)
{
    global $validStates;
    extract($inputs);
    ob_start();
    include(__DIR__ . '/template.php');
    return ob_get_clean();
}

// Validation
function validate ($inputs, $validStates)
{
    $haveErrors = false;
    $errors = array();

    if ($inputs['name'] == '') {
        $haveErrors = true;
        $errors += ['name' => 'fill name field'];
    }

    if ($inputs['email'] == '') {
        $haveErrors = true;
        $errors += ['email' => 'fill email field'];
    } else if (!emailValidation($inputs['email'])) {
        $haveErrors = true;
        $errors += ['email' => 'insert a valid email address'];
    }

    if ($inputs['state'] == '' || !stateValidation($inputs['state'], $validStates)) {
        $haveErrors = true;
        $errors += ['state' => 'select one of divisions listed'];
    }

    if ($inputs['message'] == '') {
        $haveErrors = true;
        $errors += ['message' => 'fill message field'];
    }

    $result = $haveErrors ? [
        'status' => 'failed',
        'errors' => $errors,
    ] : [
        'status' => 'success'
    ];

    return $result;
}

function emailValidation ($email)
{
    $pattern = '/^([a-zA-Z0-9_\.-]+)@([a-zA-Z0-9_\.-]+)\.([a-zA-Z]+)$/';
    return preg_match($pattern, $email);
}

function stateValidation ($state, $validStates)
{
    return array_key_exists($state, $validStates);
}
